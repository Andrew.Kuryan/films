package com.gitlab.andrewkuryan.lab1.view

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Text
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.dimensionResource
import androidx.ui.res.stringResource
import androidx.ui.text.style.TextOverflow
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.view.extensions.ImageButton

@Composable
fun StandardAppBar(
    navigateBackAction: () -> Unit,
    text: String = stringResource(R.string.appName)
) {
    val appBarHeight = dimensionResource(R.dimen.appBarHeight)

    Surface(
        color = MaterialTheme.colors().primary,
        modifier = LayoutWidth.Fill + LayoutHeight(appBarHeight),
        elevation = 5.dp
    ) {
        Row(
            modifier = LayoutHeight.Fill + LayoutPadding(
                start = 12.dp,
                end = 12.dp
            ), arrangement = Arrangement.Start
        ) {
            Container(modifier = LayoutHeight.Fill, alignment = Alignment.CenterStart) {
                ImageButton(
                    R.drawable.baseline_arrow_back_white_36dp,
                    onClick = navigateBackAction
                )
            }
            Spacer(LayoutWidth(8.dp))
            Text(
                text,
                modifier = LayoutGravity.Center,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography().h1
            )
        }
    }
}