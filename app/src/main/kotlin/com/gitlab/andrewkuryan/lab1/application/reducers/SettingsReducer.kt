package com.gitlab.andrewkuryan.lab1.application.reducers

import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetFontSize
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetFontStyle
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetLanguage
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetTheme
import org.reduxkotlin.Reducer

data class Language(val code: String)
data class FontSize(val name: String, val percent: Double)
data class FontStyle(val name: String)
data class Theme(val name: String)

val availableLanguages = listOf(
    Language("en"),
    Language("ru")
)
val availableFontSizes = listOf(
    FontSize("medium", 1.0),
    FontSize("small", 0.8),
    FontSize("large", 1.2)
)
val availableFontStyles = listOf(
    FontStyle("default"),
    FontStyle("sans_serif"),
    FontStyle("serif"),
    FontStyle("monospace"),
    FontStyle("cursive")
)
val availableThemes = listOf(
    Theme("light"),
    Theme("dark")
)

data class SettingsState(
    val language: Language = availableLanguages[0],
    val fontSize: FontSize = availableFontSizes[0],
    val fontStyle: FontStyle = availableFontStyles[0],
    val theme: Theme = availableThemes[0]
)

val settingsReducer: Reducer<SettingsState> = { state, action ->
    when (action) {
        is SetLanguage -> state.copy(language = action.language)
        is SetFontSize -> state.copy(fontSize = action.fontSize)
        is SetFontStyle -> state.copy(fontStyle = action.fontStyle)
        is SetTheme -> state.copy(theme = action.theme)
        else -> state
    }
}