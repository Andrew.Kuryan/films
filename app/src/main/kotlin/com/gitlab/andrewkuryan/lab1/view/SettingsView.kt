package com.gitlab.andrewkuryan.lab1.view

import androidx.compose.Composable
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.RadioButton
import androidx.ui.material.surface.Card
import androidx.ui.material.surface.Surface
import androidx.ui.res.stringResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.application.reducers.*

@Preview
@Composable
fun SetingsViewPreivew() {
    SettingsView({}, SettingsState(), {}, {}, {}, {})
}

@Composable
fun SettingsView(
    navigateBackAction: () -> Unit,
    settingsState: SettingsState,
    setLanguageAction: (Language) -> Unit,
    setFontSizeAction: (FontSize) -> Unit,
    setFontStyleAction: (FontStyle) -> Unit,
    setThemeAction: (Theme) -> Unit
) {
    Column {
        StandardAppBar(navigateBackAction, stringResource(R.string.settingsViewTitle))
        Surface(color = MaterialTheme.colors().background, modifier = LayoutWidth.Fill) {
            VerticalScroller(modifier = LayoutPadding(5.dp) + LayoutHeight.Fill) {
                Column {
                    LanguageSettings(settingsState, setLanguageAction)
                    Spacer(LayoutHeight(5.dp))
                    ThemeSettings(settingsState, setThemeAction)
                    Spacer(LayoutHeight(5.dp))
                    FontSettings(settingsState, setFontSizeAction, setFontStyleAction)
                }
            }
        }
    }
}

@Composable
fun LanguageSettings(settingsState: SettingsState, setLanguageAction: (Language) -> Unit) {
    SettingsBlock(R.string.languageParagraph) {
        Column {
            for ((i, lang) in availableLanguages.withIndex()) {
                SettingsItem(
                    onSelectAction = { setLanguageAction(lang) },
                    itemName = "languageItem_${lang.code}",
                    isSelected = settingsState.language == lang
                )
                if (i != availableLanguages.size) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
    }
}

@Composable
fun FontSettings(
    settingsState: SettingsState,
    setFontSizeAction: (FontSize) -> Unit,
    setFontStyleAction: (FontStyle) -> Unit
) {
    SettingsBlock(R.string.fontParagraph) {
        SettingsSubblock(R.string.fontSizeParagraph) {
            for ((i, fontSize) in availableFontSizes.withIndex()) {
                SettingsItem(
                    onSelectAction = { setFontSizeAction(fontSize) },
                    itemName = "fontSizeItem_${fontSize.name}",
                    isSelected = settingsState.fontSize == fontSize
                )
                if (i != availableFontSizes.size) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
        Spacer(LayoutHeight(5.dp))
        SettingsSubblock(R.string.fontStyleParagraph) {
            for ((i, style) in availableFontStyles.withIndex()) {
                SettingsItem(
                    onSelectAction = { setFontStyleAction(style) },
                    itemName = "fontStyleItem_${style.name}",
                    isSelected = settingsState.fontStyle == style
                )
                if (i != availableFontStyles.size) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
    }
}

@Composable
fun ThemeSettings(settingsState: SettingsState, setThemeAction: (Theme) -> Unit) {
    SettingsBlock(R.string.themeParagraph) {
        Column {
            for ((i, theme) in availableThemes.withIndex()) {
                SettingsItem(
                    onSelectAction = { setThemeAction(theme) },
                    itemName = "themeItem_${theme.name}",
                    isSelected = settingsState.theme == theme
                )
                if (i != availableThemes.size) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
    }
}

@Composable
fun SettingsBlock(titleRes: Int, content: @Composable() () -> Unit) {
    Card(LayoutWidth.Fill, elevation = 3.dp) {
        Column(LayoutPadding(8.dp)) {
            Text(
                stringResource(titleRes),
                style = MaterialTheme.typography().h2
            )
            Spacer(LayoutHeight(8.dp))
            content()
        }
    }
}

@Composable
fun SettingsSubblock(titleRes: Int, content: @Composable() () -> Unit) {
    Column {
        Text(
            stringResource(titleRes),
            style = MaterialTheme.typography().h3
        )
        Spacer(LayoutHeight(5.dp))
        content()
    }
}

@Composable
fun SettingsItem(
    onSelectAction: () -> Unit,
    itemName: String,
    isSelected: Boolean
) {
    Clickable(onClick = onSelectAction) {
        Row(LayoutWidth.Fill) {
            Container(LayoutGravity.Center) {
                RadioButton(selected = isSelected, onSelect = onSelectAction)
            }
            Spacer(LayoutWidth(8.dp))
            Text(
                stringResourceByName(itemName) ?: stringResource(R.string.languageItem_unknown),
                modifier = LayoutGravity.Center,
                style = MaterialTheme.typography().subtitle2
            )
        }
    }
}

@Composable
fun stringResourceByName(name: String): String? {
    val context = ContextAmbient.current
    val id = context.resources.getIdentifier(name, "string", context.packageName)
    return if (id == 0) null else context.getString(id)
}