package com.gitlab.andrewkuryan.lab1.view

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.CircularProgressIndicator
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.dimensionResource
import androidx.ui.res.stringResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.activity.Connect
import com.gitlab.andrewkuryan.lab1.application.actions.FILTER_VIEW_ROUTE
import com.gitlab.andrewkuryan.lab1.application.actions.Route
import com.gitlab.andrewkuryan.lab1.application.actions.SETTINGS_VIEW_ROUTE
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import com.gitlab.andrewkuryan.lab1.entity.Film
import com.gitlab.andrewkuryan.lab1.view.extensions.ImageButton

@Composable
fun FilmListAppBar(loadFilmsAction: () -> Unit, navigateToAction: (Route) -> Unit) {
    val appBarHeight = dimensionResource(R.dimen.appBarHeight)
    val appName = stringResource(R.string.appName)

    Surface(
        color = MaterialTheme.colors().primary,
        modifier = LayoutWidth.Fill + LayoutHeight(appBarHeight),
        elevation = 5.dp
    ) {
        Row(
            modifier = LayoutWidth.Fill + LayoutPadding(
                start = 16.dp,
                end = 16.dp
            ), arrangement = Arrangement.SpaceBetween
        ) {
            Text(appName, modifier = LayoutGravity.Center, style = MaterialTheme.typography().h1)
            Row(modifier = LayoutGravity.Center, arrangement = Arrangement.End) {
                ImageButton(R.drawable.baseline_refresh_white_36dp, onClick = {
                    loadFilmsAction()
                })
                Spacer(LayoutWidth(4.dp))
                ImageButton(R.drawable.baseline_filter_list_white_36dp, onClick = {
                    navigateToAction(FILTER_VIEW_ROUTE)
                })
                Spacer(LayoutWidth(4.dp))
                ImageButton(R.drawable.baseline_settings_white_36dp, onClick = {
                    navigateToAction(SETTINGS_VIEW_ROUTE)
                })
            }
        }
    }
}


@Preview
@Composable
fun FilmListViewPreview() {
    FilmListView({}, {}, {}, {}, { shouldUpdate, child -> }, ScrollerPosition())
}

@Composable
fun FilmListView(
    loadFilmsAction: () -> Unit,
    loadImageAction: (Film) -> Unit,
    navigateToAction: (Route) -> Unit,
    selectFilmAction: (Film) -> Unit,
    Connect: Connect<AppState>,
    scrollerPosition: ScrollerPosition
) {
    Column {
        FilmListAppBar(loadFilmsAction, navigateToAction)
        Surface(
            color = MaterialTheme.colors().background,
            modifier = LayoutWidth.Fill + LayoutHeight.Fill
        ) {
            Stack {
                Connect(
                    { newState, prevState ->
                        newState.filterState.filter(newState.filmListState.films).size !=
                                prevState.filterState.filter(prevState.filmListState.films).size
                    }
                ) {
                    if (it.filterState.filter(it.filmListState.films).isNotEmpty()) {
                        FilmList(
                            films = it.filterState.filter(it.filmListState.films),
                            selectFilmAction = selectFilmAction,
                            loadImageAction = loadImageAction,
                            Connect = Connect,
                            scrollerPosition = scrollerPosition
                        )
                    } else {
                        FilmListPlaceholder()
                    }
                }
                Connect(
                    { newState, prevState ->
                        newState.filmListState.isFetching != prevState.filmListState.isFetching
                    }
                ) {
                    Mask(isFetching = it.filmListState.isFetching)
                }
            }
        }
    }
}

@Composable
fun Mask(isFetching: Boolean) {
    if (isFetching) {
        Surface(
            modifier = LayoutHeight.Fill + LayoutWidth.Fill,
            elevation = 10.dp,
            color = Color(100, 100, 100, 100)
        ) {
            Center {
                CircularProgressIndicator()
            }
        }
    }
}

@Composable
fun FilmListPlaceholder() {
    Center {
        Text(
            stringResource(R.string.placeholderText),
            style = MaterialTheme.typography().caption
        )
    }
}

@Composable
fun FilmList(
    films: List<Film>,
    selectFilmAction: (Film) -> Unit,
    loadImageAction: (Film) -> Unit,
    Connect: Connect<AppState>,
    scrollerPosition: ScrollerPosition
) {
    VerticalScroller(
        scrollerPosition = scrollerPosition,
        modifier = LayoutPadding(5.dp)
    ) {
        Column {
            for (i in films.indices) {
                FilmItemView(
                    Connect = Connect,
                    index = i,
                    selectFilmAction = selectFilmAction,
                    loadImageAction = loadImageAction
                )
                if (i != films.size - 1) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
    }
}