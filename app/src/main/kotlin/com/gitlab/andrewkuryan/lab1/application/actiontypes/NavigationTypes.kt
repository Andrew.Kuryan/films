package com.gitlab.andrewkuryan.lab1.application.actiontypes

import com.gitlab.andrewkuryan.lab1.application.actions.Route

sealed class NavigationAction : AppAction()

data class NavigateTo(val route: Route) : NavigationAction()

data class NavigateWithReplace(val route: Route) : NavigationAction()

object NavigateBack : NavigationAction()