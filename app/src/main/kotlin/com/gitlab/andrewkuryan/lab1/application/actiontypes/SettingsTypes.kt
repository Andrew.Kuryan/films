package com.gitlab.andrewkuryan.lab1.application.actiontypes

import com.gitlab.andrewkuryan.lab1.application.reducers.FontSize
import com.gitlab.andrewkuryan.lab1.application.reducers.FontStyle
import com.gitlab.andrewkuryan.lab1.application.reducers.Language
import com.gitlab.andrewkuryan.lab1.application.reducers.Theme

sealed class SettingsAction : AppAction()

data class SetLanguage(val language: Language) : SettingsAction()

data class SetFontSize(val fontSize: FontSize) : SettingsAction()

data class SetFontStyle(val fontStyle: FontStyle) : SettingsAction()

data class SetTheme(val theme: Theme) : SettingsAction()