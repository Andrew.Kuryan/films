package com.gitlab.andrewkuryan.lab1.view.extensions

import androidx.compose.Composable
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.dimensionResource
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R

@Composable
fun ModalDialog(
    title: String,
    closeButtonText: String,
    onClose: () -> Unit = {},
    content: @Composable() () -> Unit
) {
    val appBarHeight = dimensionResource(R.dimen.appBarHeight)
    val screenHeightDp = ContextAmbient.current.resources.configuration.screenHeightDp
    val screenWidthDp = ContextAmbient.current.resources.configuration.screenWidthDp

    val dialogHeight = (screenHeightDp - appBarHeight.value) * 0.8
    val dialogWidth = screenWidthDp * 0.85

    Surface(
        modifier = LayoutHeight.Fill + LayoutWidth.Fill + LayoutPadding(
            top = appBarHeight,
            bottom = 0.dp,
            start = 0.dp,
            end = 0.dp
        ),
        elevation = 10.dp,
        color = Color(100, 100, 100, 100)
    ) {
        Center {
            Surface(
                modifier = LayoutHeight(dialogHeight.dp) + LayoutWidth(dialogWidth.dp),
                elevation = 8.dp,
                color = MaterialTheme.colors().background
            ) {
                Column(LayoutPadding(10.dp)) {
                    Text(
                        modifier = LayoutHeight(28.dp),
                        text = title,
                        style = MaterialTheme.typography().h2
                    )
                    Spacer(LayoutHeight(8.dp))
                    VerticalScroller(modifier = LayoutHeight((dialogHeight - 28 - 40 - 20 - 8).dp)) {
                        content()
                    }
                    Button(LayoutHeight(40.dp) + LayoutGravity.End, onClick = onClose) {
                        Text(closeButtonText, style = MaterialTheme.typography().button)
                    }
                }
            }
        }
    }
}