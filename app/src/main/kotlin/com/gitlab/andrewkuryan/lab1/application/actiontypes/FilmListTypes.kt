package com.gitlab.andrewkuryan.lab1.application.actiontypes

import com.gitlab.andrewkuryan.lab1.entity.Film

sealed class FilmListAction : AppAction()

object LoadFilmsFetching : FilmListAction()

data class LoadFilmsSuccess(val films: List<Film>) : FilmListAction()

data class LoadFilmsFailed(val error: Exception) : FilmListAction()

object ResetError : FilmListAction()

data class SelectFilm(val film: Film?) : FilmListAction()

data class SetImage(val film: Film, val image: ByteArray) : FilmListAction() {
    override fun equals(other: Any?) =
        (javaClass == other?.javaClass)
                && image.contentEquals((other as SetImage).image)
                && film shallowEquals other.film

    override fun hashCode() = image.contentHashCode() * 31 + film.hashCode()
}

data class SetImageError(val film: Film, val error: Throwable) : FilmListAction()