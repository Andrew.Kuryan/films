package com.gitlab.andrewkuryan.lab1.application.actions

import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateBack
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateTo
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateWithReplace
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import org.reduxkotlin.Store

typealias Route = String

const val FILM_LIST_VIEW_ROUTE = "FilmListViewRoute"
const val FILM_DETAILS_VIEW_ROUTE = "FilmDetailsViewRoute"
const val FILTER_VIEW_ROUTE = "FilterViewRoute"
const val SETTINGS_VIEW_ROUTE = "SettingsViewRoute"

fun navigateTo(store: Store<AppState>): (Route) -> Unit {
    return { route ->
        store.dispatch(NavigateTo(route))
    }
}

fun navigateWithReplace(store: Store<AppState>): (Route) -> Unit {
    return { route ->
        store.dispatch(NavigateWithReplace(route))
    }
}

fun navigateBack(store: Store<AppState>): () -> Unit {
    return {
        store.dispatch(NavigateBack)
    }
}