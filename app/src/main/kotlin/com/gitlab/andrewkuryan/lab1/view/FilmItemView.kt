package com.gitlab.andrewkuryan.lab1.view

import android.graphics.BitmapFactory
import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.core.toModifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Clickable
import androidx.ui.graphics.Image
import androidx.ui.graphics.ImageConfig
import androidx.ui.graphics.ScaleFit
import androidx.ui.graphics.colorspace.ColorSpaces
import androidx.ui.graphics.imageFromResource
import androidx.ui.graphics.painter.ImagePainter
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.ripple.Ripple
import androidx.ui.material.surface.Card
import androidx.ui.text.style.TextOverflow
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.activity.Connect
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import com.gitlab.andrewkuryan.lab1.entity.Film

class RemoteImage(bytes: ByteArray) : Image {

    private val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)

    override val width = bitmap.width
    override val height = bitmap.height
    override val config = ImageConfig.Argb8888
    override val colorSpace = ColorSpaces.Srgb
    override val hasAlpha = bitmap.hasAlpha()
    override val nativeImage = bitmap
    override fun prepareToDraw() = bitmap.prepareToDraw()
}

@Composable
fun FilmItemView(
    Connect: Connect<AppState>,
    selectFilmAction: (Film) -> Unit,
    loadImageAction: (Film) -> Unit,
    index: Int
) {
    val context = ContextAmbient.current

    Ripple(bounded = true) {
        Card(LayoutWidth.Fill + LayoutHeight(108.dp), elevation = 3.dp) {
            Connect(
                { newState, prevState ->
                    newState.filterState.filter(newState.filmListState.films)[index] !=
                            prevState.filterState.filter(prevState.filmListState.films)[index]
                }
            ) {
                val film = it.filterState.filter(it.filmListState.films)[index]

                Clickable(onClick = { selectFilmAction(film) }) {
                    Row(
                        modifier = LayoutWidth.Fill + LayoutHeight(92.dp) + LayoutPadding(
                            top = 8.dp,
                            bottom = 8.dp,
                            start = 4.dp,
                            end = 4.dp
                        )
                    ) {
                        val preview = if (film.image == null) {
                            if (film.imageError == null) {
                                loadImageAction(film)
                            }
                            imageFromResource(context.resources, R.drawable.movie)
                        } else {
                            RemoteImage(film.image)
                        }

                        Container(modifier = LayoutHeight.Fill, alignment = Alignment.Center) {
                            Box(
                                LayoutSize(
                                    72.dp,
                                    92.dp
                                ) + ImagePainter(preview).toModifier(scaleFit = ScaleFit.FillHeight)
                            )
                        }
                        Spacer(LayoutWidth(10.dp))
                        Column(
                            modifier = LayoutHeight.Fill,
                            arrangement = Arrangement.SpaceBetween
                        ) {
                            Column(arrangement = Arrangement.Top) {
                                Text(
                                    film.title,
                                    style = MaterialTheme.typography().h4,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 2
                                )
                                Text(film.year.toString(), style = MaterialTheme.typography().body1)
                            }
                            Text(
                                "imdb: ${film.imdbRating}/10",
                                style = MaterialTheme.typography().body1
                            )
                        }
                    }
                }
            }
        }
    }
}