package com.gitlab.andrewkuryan.lab1.view

import androidx.compose.Composable
import androidx.compose.state
import androidx.ui.core.Text
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.*
import androidx.ui.material.*
import androidx.ui.material.surface.Card
import androidx.ui.material.surface.Surface
import androidx.ui.res.stringResource
import androidx.ui.text.style.TextAlign
import androidx.ui.text.style.TextOverflow
import androidx.ui.unit.Dp
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.activity.Connect
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import com.gitlab.andrewkuryan.lab1.view.extensions.ModalDialog
import kotlin.math.round

@Composable
fun FilterView(
    Connect: Connect<AppState>,
    setMinYearAction: (Int) -> Unit,
    setMaxYearAction: (Int) -> Unit,
    setMinRatingAction: (Float) -> Unit,
    setMaxRatingAction: (Float) -> Unit,
    setUserCountriesAction: (List<String>) -> Unit,
    setUserGenresAction: (List<String>) -> Unit,
    setUserProductionAction: (List<String>) -> Unit,
    navigateBackAction: () -> Unit
) {
    Stack {
        Connect(
            { newState, prevState ->
                newState.filterState.userCountries != prevState.filterState.userCountries
                        || newState.filterState.userGenres != prevState.filterState.userGenres
                        || newState.filterState.userProduction != prevState.filterState.userProduction
            }
        ) { filteredState ->
            val (countriesDialogShowing, setCountriesDialogShowing) = state { false }
            val (genresDialogShowing, setGenresDialogShowing) = state { false }
            val (productionDialogShowing, setProductionDialogShowing) = state { false }

            val filterState = filteredState.filterState

            Column {
                StandardAppBar(navigateBackAction, stringResource(R.string.filterViewTitle))
                Surface(color = MaterialTheme.colors().background, modifier = LayoutWidth.Fill) {
                    VerticalScroller(modifier = LayoutPadding(5.dp) + LayoutHeight.Fill) {
                        Column {
                            FilterBlock(R.string.yearFilterParagraph) {
                                Column {
                                    Text(stringResource(R.string.rangeStartTitle))
                                    TitledSlider(
                                        filterState.yearUserRange.first.toFloat(),
                                        filterState.yearRange.first.toFloat(),
                                        filterState.yearRange.last.toFloat(),
                                        filterState.yearRange.last - filterState.yearRange.first + 1,
                                        formatter = { it.toInt().toString() },
                                        onChange = { setMinYearAction(it.toInt()) }
                                    )
                                    Text(stringResource(R.string.rangeEndTitle))
                                    TitledSlider(
                                        filterState.yearUserRange.last.toFloat(),
                                        filterState.yearRange.first.toFloat(),
                                        filterState.yearRange.last.toFloat(),
                                        filterState.yearRange.last - filterState.yearRange.first + 1,
                                        formatter = { it.toInt().toString() },
                                        onChange = { setMaxYearAction(it.toInt()) }
                                    )
                                }
                            }
                            Spacer(LayoutHeight(5.dp))
                            FilterBlock(R.string.ratingFilterParagraph) {
                                Column {
                                    Text(stringResource(R.string.rangeStartTitle))
                                    TitledSlider(
                                        filterState.ratingUserRange.start,
                                        filterState.ratingRange.start,
                                        filterState.ratingRange.endInclusive,
                                        ((filterState.ratingRange.endInclusive - filterState.ratingRange.start).toInt() + 1) * 10,
                                        formatter = { String.format("%.1f", it) },
                                        onChange = { setMinRatingAction(round(it * 10.0f) / 10.0f) }
                                    )
                                    Text(stringResource(R.string.rangeEndTitle))
                                    TitledSlider(
                                        filterState.ratingUserRange.endInclusive,
                                        filterState.ratingRange.start,
                                        filterState.ratingRange.endInclusive,
                                        ((filterState.ratingRange.endInclusive - filterState.ratingRange.start).toInt() + 1) * 10,
                                        formatter = { String.format("%.1f", it) },
                                        onChange = { setMaxRatingAction(round(it * 10.0f) / 10.0f) }
                                    )
                                }
                            }
                            Spacer(LayoutHeight(5.dp))
                            FilterBlock(R.string.countryFilterParagraph, paddingBottom = 5.dp) {
                                StringSetFilterButton(
                                    onClick = { setCountriesDialogShowing(true) },
                                    stringSet = filterState.userCountries
                                )
                            }
                            Spacer(LayoutHeight(5.dp))
                            FilterBlock(R.string.genreFilterParagraph, paddingBottom = 5.dp) {
                                StringSetFilterButton(
                                    onClick = { setGenresDialogShowing(true) },
                                    stringSet = filterState.userGenres
                                )
                            }
                            Spacer(LayoutHeight(5.dp))
                            FilterBlock(R.string.productionFilterParagraph, paddingBottom = 5.dp) {
                                StringSetFilterButton(
                                    onClick = { setProductionDialogShowing(true) },
                                    stringSet = filterState.userProduction
                                )
                            }
                        }
                    }
                }
            }
            if (countriesDialogShowing) {
                StringSetFilterModal(
                    titleRes = R.string.countryFilterParagraph,
                    onClose = { setCountriesDialogShowing(false) },
                    onChangeAction = setUserCountriesAction,
                    availableSet = filterState.availableCountries,
                    stringSet = filterState.userCountries
                )
            }
            if (genresDialogShowing) {
                StringSetFilterModal(
                    titleRes = R.string.genreFilterParagraph,
                    onClose = { setGenresDialogShowing(false) },
                    onChangeAction = setUserGenresAction,
                    availableSet = filterState.availableGenres,
                    stringSet = filterState.userGenres
                )
            }
            if (productionDialogShowing) {
                StringSetFilterModal(
                    titleRes = R.string.productionFilterParagraph,
                    onClose = { setProductionDialogShowing(false) },
                    onChangeAction = setUserProductionAction,
                    availableSet = filterState.availableProduction,
                    stringSet = filterState.userProduction
                )
            }
        }
    }
}

@Composable
fun FilterBlock(titleRes: Int, paddingBottom: Dp = 0.dp, content: @Composable() () -> Unit) {
    Card(LayoutWidth.Fill, elevation = 3.dp) {
        Column(
            LayoutPadding(
                top = 8.dp,
                start = 8.dp,
                end = 8.dp,
                bottom = (8.dp + paddingBottom)
            )
        ) {
            Text(
                stringResource(titleRes),
                style = MaterialTheme.typography().h2
            )
            Spacer(LayoutHeight(8.dp))
            content()
        }
    }
}

@Composable
fun StringSetFilterButton(onClick: () -> Unit, stringSet: List<String>) {
    OutlinedButton(LayoutHeight(60.dp) + LayoutWidth.Fill, onClick = onClick) {
        Text(
            stringSet.joinToString(separator = ", "),
            style = MaterialTheme.typography().button
                .copy(color = MaterialTheme.colors().secondary),
            maxLines = 2,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Composable
fun StringSetFilterModal(
    titleRes: Int,
    onClose: () -> Unit,
    onChangeAction: (List<String>) -> Unit,
    availableSet: List<String>,
    stringSet: List<String>
) {
    ModalDialog(
        title = stringResource(titleRes),
        closeButtonText = stringResource(R.string.closeButtonText),
        onClose = onClose
    ) {
        Column {
            for ((i, item) in availableSet.withIndex()) {
                val onCheckedChange = { value: Boolean ->
                    if (!value) {
                        val newSet = stringSet - item
                        onChangeAction(availableSet.filter { it in newSet })
                    } else {
                        val newSet = stringSet + item
                        onChangeAction(availableSet.filter { it in newSet })
                    }
                }

                Clickable(onClick = { onCheckedChange(item !in stringSet) }) {
                    Row(LayoutWidth.Fill) {
                        Checkbox(
                            checked = item in stringSet,
                            modifier = LayoutGravity.Center,
                            onCheckedChange = onCheckedChange
                        )
                        Spacer(LayoutWidth(5.dp))
                        Text(
                            item,
                            modifier = LayoutGravity.Center,
                            style = MaterialTheme.typography().subtitle2
                        )
                    }
                }

                if (i != availableSet.size - 1) {
                    Spacer(LayoutHeight(5.dp))
                }
            }
        }
    }
}

@Composable
fun TitledSlider(
    initPosition: Float,
    start: Float,
    end: Float,
    steps: Int,
    formatter: (Float) -> String,
    onChange: (Float) -> Unit
) {
    Row {
        val position = SliderPosition(
            initPosition,
            start..end,
            steps = steps
        )
        Slider(position, modifier = LayoutGravity.Center + LayoutFlexible(0.85f),
            onValueChangeEnd = {
                onChange(position.value)
            })
        Spacer(LayoutWidth(5.dp))
        Text(
            formatter(position.value),
            modifier = LayoutGravity.Center + LayoutFlexible(0.15f),
            maxLines = 1,
            style = MaterialTheme.typography().h4.copy(textAlign = TextAlign.End)
        )
    }
}