package com.gitlab.andrewkuryan.lab1.entity

data class Film(
    val title: String,
    val year: Int,
    val countryNames: List<String>,
    val duration: Int,
    val coverUri: String? = null,
    val imdbRating: Float,
    val actors: List<String>,
    val director: String,
    val plot: String,
    val genres: List<String>,
    val production: String,
    val trailerUri: String? = null,
    @Transient
    val image: ByteArray? = null,
    @Transient
    val imageError: Throwable? = null
) {

    infix fun shallowEquals(other: Film?) = other != null &&
            title == other.title && year == other.year

    infix fun notShallowEquals(other: Film?) = !shallowEquals(other)

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + countryNames.hashCode()
        result = 31 * result + duration
        result = 31 * result + (coverUri?.hashCode() ?: 0)
        result = 31 * result + imdbRating.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Film

        if (title != other.title) return false
        if (year != other.year) return false
        if (image != null) {
            if (other.image == null) return false
            if (!image.contentEquals(other.image)) return false
        } else if (other.image != null) return false

        return true
    }

    override fun toString() =
        "Film(title=$title, year=$year, image=${if (image == null) "NULL" else "IMAGE"})"
}