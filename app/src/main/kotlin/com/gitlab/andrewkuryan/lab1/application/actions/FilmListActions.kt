package com.gitlab.andrewkuryan.lab1.application.actions

import com.gitlab.andrewkuryan.lab1.application.actiontypes.*
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import com.gitlab.andrewkuryan.lab1.application.reducers.FilmListState
import com.gitlab.andrewkuryan.lab1.entity.Film
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import kotlinx.coroutines.*
import org.reduxkotlin.Store
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.TimeoutException


val testFilms = listOf(
    Film(
        "Star Wars: Episode IX - The Rise of Skywalker",
        2019,
        listOf("USA"),
        142,
        coverUri = "https://m.media-amazon.com/images/M/MV5BMDljNTQ5ODItZmQwMy00M2ExLTljOTQtZTVjNGE2NTg0NGIxXkEyXkFqcGdeQXVyODkzNTgxMDg@._V1_SX300.jpg",
        imdbRating = 6.9F,
        actors = listOf("Carrie Fisher", "Mark Hamill", "Adam Driver", "Daisy Ridley"),
        director = "J.J. Abrams",
        plot = "The surviving members of the resistance face the First Order once again, and the legendary conflict between the Jedi and the Sith reaches its peak bringing the Skywalker saga to its end.",
        genres = listOf("Action", "Adventure", "Fantasy", "Sci-Fi"),
        production = "Walt Disney Pictures",
        trailerUri = "https://firebasestorage.googleapis.com/v0/b/lab1-e7222.appspot.com/o/Star%20Wars%3A%20Episode%20IX%20-%20The%20Rise%20of%20Skywalker.mp4?alt=media&token=7d3fbfdb-aa8a-4c65-ab1c-031c6ed482d4"
    ),
    Film(
        "The Hobbit: An Unexpected Journey",
        2012,
        listOf("USA", "New Zealand"),
        169,
        coverUri = "https://m.media-amazon.com/images/M/MV5BMTcwNTE4MTUxMl5BMl5BanBnXkFtZTcwMDIyODM4OA@@._V1_SX300.jpg",
        imdbRating = 7.8F,
        actors = listOf("Ian McKellen", "Martin Freeman", "Richard Armitage", "Ken Stott"),
        director = "Peter Jackson",
        plot = "Bilbo Baggins is swept into a quest to reclaim the lost Dwarf Kingdom of Erebor from the fearsome dragon Smaug. Approached out of the blue by the wizard Gandalf the Grey, Bilbo finds himself joining a company of thirteen dwarves led by the legendary warrior, Thorin Oakenshield. Their journey will take them into the Wild; through treacherous lands swarming with Goblins and Orcs, deadly Wargs and Giant Spiders, Shapeshifters and Sorcerers. Although their goal lies to the East and the wastelands of the Lonely Mountain first they must escape the goblin tunnels, where Bilbo meets the creature that will change his life forever ... Gollum. Here, alone with Gollum, on the shores of an underground lake, the unassuming Bilbo Baggins not only discovers depths of guile and courage that surprise even him, he also gains possession of Gollum's \"precious\" ring that holds unexpected and useful qualities ... A simple, gold ring that is tied to the fate of all Middle-earth in ways Bilbo cannot begin to know.",
        genres = listOf("Adventure", "Family", "Fantasy"),
        production = "Warner Bros."
    ),
    Film(
        "Mission: Impossible - Fallout",
        2018,
        listOf("USA", "China", "France", "Norway", "Great Britain"),
        147,
        coverUri = "https://m.media-amazon.com/images/M/MV5BNjRlZmM0ODktY2RjNS00ZDdjLWJhZGYtNDljNWZkMGM5MTg0XkEyXkFqcGdeQXVyNjAwMjI5MDk@._V1_SX300.jpg",
        imdbRating = 7.8F,
        actors = listOf("Tom Cruise", "Henry Cavill", "Ving Rhames", "Simon Pegg"),
        director = "Christopher McQuarrie",
        plot = "Ethan Hunt and his IMF team, along with some familiar allies, race against time after a mission gone wrong.",
        genres = listOf("Action", "Adventure", "Thriller"),
        production = "Paramount Pictures"
    )
)

fun initFilmList(loadFilmsAction: () -> Unit): (FilmListState) -> Unit {
    return { state ->
        if (state.films.isEmpty()) {
            loadFilmsAction()
        }
    }
}

fun loadFilms(
    store: Store<AppState>,
    database: FirebaseDatabase,
    loadFilmsErrorAction: (Exception) -> Unit
): () -> Unit {
    val listener = SingleValueEventListener(store, loadFilmsErrorAction)
    return {
        store.dispatch(LoadFilmsFetching)
        listener.status = SingleValueEventListener.Status.READY
        database.getReference("/").root.addListenerForSingleValueEvent(listener)
        setTimeout(15_000L) {
            if (listener.status == SingleValueEventListener.Status.READY) {
                database.getReference("/").removeEventListener(listener)
                CoroutineScope(Dispatchers.Main).launch {
                    loadFilmsErrorAction(TimeoutException())
                }
            }
        }
    }
}

fun loadFilmsError(store: Store<AppState>): (Exception) -> Unit {
    return { error ->
        store.dispatch(LoadFilmsFailed(error))
        setTimeout(3000) {
            CoroutineScope(Dispatchers.Main).launch {
                store.dispatch(ResetError)
            }
        }
    }
}

fun resetFilmsError(store: Store<AppState>): () -> Unit {
    return {
        store.dispatch(ResetError)
    }
}

fun loadImage(store: Store<AppState>): (Film) -> Unit {
    return { film ->
        if (film.coverUri != null) {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val bytes = loadByteArrayAsync(film.coverUri).await()
                    CoroutineScope(Dispatchers.Main).launch {
                        store.dispatch(SetImage(film, bytes))
                    }
                } catch (exc: Exception) {
                    CoroutineScope(Dispatchers.Main).launch {
                        store.dispatch(SetImageError(film, exc))
                    }
                }
            }
        }
    }
}

fun selectFilm(store: Store<AppState>): (Film) -> Unit {
    return { film ->
        store.dispatch(SelectFilm(film))
        store.dispatch(NavigateTo(FILM_DETAILS_VIEW_ROUTE))
    }
}

class SingleValueEventListener(
    private val store: Store<AppState>,
    private val loadFilmsErrorAction: (Exception) -> Unit
) : ValueEventListener {

    enum class Status {
        READY, COMPLETED, FAILED
    }

    var status = Status.READY

    override fun onCancelled(error: DatabaseError) {
        CoroutineScope(Dispatchers.Main).launch {
            loadFilmsErrorAction(Exception(error.message))
            status = Status.FAILED
        }
    }

    override fun onDataChange(data: DataSnapshot) {
        CoroutineScope(Dispatchers.Main).launch {
            val films = parseFilms(data)
            store.dispatch(LoadFilmsSuccess(films))
            store.dispatch(InitFilter(films))
            status = Status.COMPLETED
        }
    }
}

private fun loadByteArrayAsync(uri: String): Deferred<ByteArray> =
    CoroutineScope(Dispatchers.IO).async {
        withContext(Dispatchers.IO) {
            with(URL(uri).openConnection() as HttpURLConnection) {
                doInput = true
                connect()
                inputStream.readBytes()
            }
        }
    }

fun parseFilms(data: DataSnapshot) = data.children.mapNotNull { it.toFilm() }

private fun DataSnapshot.toFilm(): Film? {
    return try {
        Film(
            title = child("Title").getValue<String>()!!,
            year = child("Year").getValue<String>()!!.toInt(),
            countryNames = child("Country").getValue<String>()!!.split(", "),
            duration = child("Runtime").getValue<String>()!!.removeSuffix(" min").toInt(),
            coverUri = child("Poster").getValue<String>(),
            imdbRating = child("imdbRating").getValue<String>()!!.toFloat(),
            actors = child("Actors").getValue<String>()!!.split(", "),
            director = child("Director").getValue<String>()!!,
            plot = child("Plot").getValue<String>()!!,
            genres = child("Genre").getValue<String>()!!.split(", "),
            production = child("Production").getValue<String>()!!,
            trailerUri = child("Trailer").getValue<String>()!!.run { if (isEmpty()) null else this }
        )
    } catch (exc: Exception) {
        null
    }
}

fun setTimeout(delay: Long, action: () -> Unit) {
    val timer = Timer()
    val timerTask = object : TimerTask() {
        override fun run() {
            timer.cancel()
            action()
        }
    }
    timer.schedule(timerTask, delay)
}