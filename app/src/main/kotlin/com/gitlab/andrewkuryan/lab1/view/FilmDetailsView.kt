package com.gitlab.andrewkuryan.lab1.view

import androidx.compose.Composable
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.core.toModifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.ScaleFit
import androidx.ui.graphics.imageFromResource
import androidx.ui.graphics.painter.ImagePainter
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.dimensionResource
import androidx.ui.res.stringResource
import androidx.ui.text.style.TextAlign
import androidx.ui.text.style.TextOverflow
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.application.actions.testFilms
import com.gitlab.andrewkuryan.lab1.entity.Film
import com.gitlab.andrewkuryan.lab1.view.extensions.FlatButton

@Preview
@Composable
fun FilmDetailsPreview() {
    FilmDetailsView({}, {}, testFilms.first(), {})
}

@Composable
fun FilmDetailsView(
    navigateBackAction: () -> Unit,
    loadImageAction: (Film) -> Unit,
    selectedFilm: Film,
    startTrailerActivityAction: (String) -> Unit
) {
    val context = ContextAmbient.current

    val appBarHeight = dimensionResource(R.dimen.appBarHeight)
    val screenHeightDp = context.resources.configuration.screenHeightDp
    val scrollerHeight = if (selectedFilm.trailerUri != null) {
        screenHeightDp - appBarHeight.value - 50
    } else {
        screenHeightDp - appBarHeight.value
    }

    val preview = if (selectedFilm.image == null) {
        if (selectedFilm.imageError == null) {
            loadImageAction(selectedFilm)
        }
        imageFromResource(context.resources, R.drawable.movie)
    } else {
        RemoteImage(selectedFilm.image)
    }

    Column {
        StandardAppBar(navigateBackAction, selectedFilm.title)
        Surface(color = MaterialTheme.colors().background, modifier = LayoutWidth.Fill) {
            VerticalScroller(modifier = LayoutHeight(scrollerHeight.dp)) {
                Column(modifier = LayoutPadding(8.dp) + LayoutWidth.Fill) {
                    Row {
                        Box(
                            LayoutSize(
                                144.dp,
                                196.dp
                            ) + ImagePainter(preview).toModifier(scaleFit = ScaleFit.FillHeight)
                        )
                        Row(LayoutWidth.Fill, arrangement = Arrangement.End) {
                            PeopleInfo(selectedFilm)
                        }
                    }
                    Spacer(LayoutHeight(10.dp))
                    Text(
                        selectedFilm.title,
                        modifier = LayoutWidth.Fill,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis,
                        style = MaterialTheme.typography().caption.copy(textAlign = TextAlign.Center)
                    )
                    Spacer(LayoutHeight(4.dp))
                    Text(
                        text = selectedFilm.plot,
                        style = MaterialTheme.typography().body1.copy(textAlign = TextAlign.Center)
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.yearParagraph),
                        selectedFilm.year.toString()
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.countryParagraph),
                        selectedFilm.countryNames.joinToString(", ")
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.genreParagraph),
                        selectedFilm.genres.joinToString(", ")
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.durationParagraph),
                        "${selectedFilm.duration} ${stringResource(R.string.durationUnit)}"
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.ratingParagraph),
                        "${selectedFilm.imdbRating}/10"
                    )
                    SecondaryFilmInfo(
                        stringResource(R.string.productionParagraph),
                        selectedFilm.production
                    )
                }
            }
        }
        if (selectedFilm.trailerUri != null) {
            FlatButton(
                stringResource(R.string.trailerButtonText),
                modifier = LayoutWidth.Fill + LayoutHeight(50.dp),
                onClick = { startTrailerActivityAction(selectedFilm.trailerUri) },
                graphics = imageFromResource(
                    context.resources,
                    R.drawable.baseline_local_movies_white_36dp
                )
            )
        }
    }
}

@Composable
fun PeopleInfo(film: Film) {
    Column {
        Text(
            "${stringResource(R.string.actorsParagraph)}:",
            style = MaterialTheme.typography().subtitle1
        )
        for (actor in film.actors) {
            Text(actor, style = MaterialTheme.typography().body1)
        }
        Spacer(LayoutHeight(5.dp))
        Text(
            "${stringResource(R.string.directorParagraph)}:",
            style = MaterialTheme.typography().subtitle1
        )
        Text(film.director, style = MaterialTheme.typography().body1)
    }
}

@Composable
fun SecondaryFilmInfo(key: String, value: String) {
    Spacer(LayoutHeight(4.dp))
    Row(modifier = LayoutWidth.Fill, arrangement = Arrangement.SpaceBetween) {
        Text("$key:", style = MaterialTheme.typography().subtitle1.copy(textAlign = TextAlign.Left))
        Spacer(LayoutWidth(8.dp))
        Text(value, style = MaterialTheme.typography().body1.copy(textAlign = TextAlign.Right))
    }
}