package com.gitlab.andrewkuryan.lab1.application.middleware

import com.appspot.magtech.httplogger.HttpLogger
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import org.reduxkotlin.middleware

val loggerMiddleware = { logger: HttpLogger ->
    middleware<AppState> { store, next, action ->
        val result = next(action)
        logger.debug("NEXT STATE: ${store.state}")
        result
    }
}