package com.gitlab.andrewkuryan.lab1.application.reducers

import com.gitlab.andrewkuryan.lab1.application.actiontypes.*
import com.gitlab.andrewkuryan.lab1.entity.Film
import org.reduxkotlin.Reducer

data class FilterState(
    val yearRange: IntRange = 0..2020,
    val yearUserRange: IntRange = 0..2020,
    val ratingRange: ClosedFloatingPointRange<Float> = (0.0f)..(10.0f),
    val ratingUserRange: ClosedFloatingPointRange<Float> = (0.0f)..(10.0f),
    val availableCountries: List<String> = listOf(),
    val userCountries: List<String> = listOf(),
    val availableGenres: List<String> = listOf(),
    val userGenres: List<String> = listOf(),
    val availableProduction: List<String> = listOf(),
    val userProduction: List<String> = listOf()
) {

    fun filter(films: List<Film>) = films.filter { matches(it) }

    private fun matches(film: Film) = yearUserRange.contains(film.year)
            && ratingUserRange.contains(film.imdbRating)
            && film.countryNames.any { userCountries.contains(it) }
            && film.genres.any { userGenres.contains(it) }
            && film.production in userProduction
}

val filterReducer: Reducer<FilterState> = { state, action ->
    when (action) {
        is InitFilter -> {
            val yearRange = action.filmList.yearRange()
            val ratingRange = action.filmList.ratingRange()
            val countries = action.filmList.map { it.countryNames }.flatten().distinct()
            val genres = action.filmList.map { it.genres }.flatten().distinct()
            val production = action.filmList.map { it.production }.distinct()
            state.copy(
                yearRange = yearRange,
                yearUserRange = yearRange,
                ratingRange = ratingRange,
                ratingUserRange = ratingRange,
                availableCountries = countries,
                userCountries = countries,
                availableGenres = genres,
                userGenres = genres,
                availableProduction = production,
                userProduction = production
            )
        }

        is SetMinYear -> state.copy(
            yearUserRange = action.year..state.yearUserRange.last
        )
        is SetMaxYear -> state.copy(
            yearUserRange = state.yearUserRange.first..action.year
        )
        is SetMinRating -> state.copy(
            ratingUserRange = action.rating..state.ratingUserRange.endInclusive
        )
        is SetMaxRating -> state.copy(
            ratingUserRange = state.ratingUserRange.start..action.rating
        )

        is SetUserCountries -> state.copy(userCountries = action.countries)
        is SetUserGenres -> state.copy(userGenres = action.genres)
        is SetUserProduction -> state.copy(userProduction = action.production)
        else -> state
    }
}

private fun List<Film>.yearRange() =
    (minBy { it.year }?.year ?: 0)..(maxBy { it.year }?.year ?: 2020)

private fun List<Film>.ratingRange() =
    (minBy { it.imdbRating }?.imdbRating ?: 0.0f)..(maxBy { it.imdbRating }?.imdbRating ?: 10.0f)