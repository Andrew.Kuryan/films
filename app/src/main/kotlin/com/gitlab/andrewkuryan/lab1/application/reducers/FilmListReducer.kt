package com.gitlab.andrewkuryan.lab1.application.reducers

import com.gitlab.andrewkuryan.lab1.application.actiontypes.*
import com.gitlab.andrewkuryan.lab1.entity.Film
import org.reduxkotlin.Reducer

data class FilmListState(
    val isFetching: Boolean = false,
    val films: List<Film> = listOf(),
    val error: Exception? = null,
    val selectedFilm: Film? = null
)

val filmListReducer: Reducer<FilmListState> = { state, action ->
    when (action) {
        is LoadFilmsFetching -> state.copy(isFetching = true)
        is LoadFilmsSuccess -> state.copy(isFetching = false, films = action.films, error = null)
        is LoadFilmsFailed -> state.copy(isFetching = false, error = action.error)
        is ResetError -> state.copy(error = null)
        is SelectFilm -> state.copy(selectedFilm = action.film)

        is SetImage -> state.updateFilms(action.film) { it.copy(image = action.image) }
        is SetImageError -> state.updateFilms(action.film) { it.copy(imageError = action.error) }
        else -> state
    }
}

private fun FilmListState.updateFilms(film: Film, action: (Film) -> Film): FilmListState {
    return copy(
        films = films.map {
            if (it notShallowEquals film) it else action(it)
        },
        selectedFilm = if (film notShallowEquals selectedFilm) {
            selectedFilm
        } else action(selectedFilm!!)
    )
}