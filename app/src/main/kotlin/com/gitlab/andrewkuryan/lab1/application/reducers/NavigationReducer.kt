package com.gitlab.andrewkuryan.lab1.application.reducers

import com.gitlab.andrewkuryan.lab1.application.actions.FILM_LIST_VIEW_ROUTE
import com.gitlab.andrewkuryan.lab1.application.actions.Route
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateBack
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateTo
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigateWithReplace
import org.reduxkotlin.Reducer

data class NavigationState(
    val currentRoute: Route = FILM_LIST_VIEW_ROUTE,
    val history: List<Route> = listOf(FILM_LIST_VIEW_ROUTE)
)

val navigationReducer: Reducer<NavigationState> = { state, action ->
    when (action) {
        is NavigateTo -> state.copy(
            currentRoute = action.route,
            history = state.history + action.route
        )
        is NavigateWithReplace -> state.copy(
            currentRoute = action.route,
            history = state.history.dropLast(1) + action.route
        )
        is NavigateBack -> state.copy(
            currentRoute = state.history[state.history.size - 2],
            history = state.history.dropLast(1)
        )
        else -> state
    }
}