package com.gitlab.andrewkuryan.lab1.view.extensions

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.core.Text
import androidx.ui.core.toModifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Clickable
import androidx.ui.graphics.Color
import androidx.ui.graphics.Image
import androidx.ui.graphics.painter.ImagePainter
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.ripple.Ripple
import androidx.ui.material.surface.Surface
import androidx.ui.text.style.TextOverflow
import androidx.ui.unit.dp

@Composable
fun FlatButton(
    text: String,
    modifier: Modifier,
    onClick: () -> Unit = {},
    color: Color = MaterialTheme.colors().primary,
    graphics: Image? = null
) {
    Ripple(bounded = true) {
        Clickable(onClick = onClick) {
            Surface(
                modifier = modifier,
                color = color
            ) {
                Center {
                    Row(LayoutPadding(top = 0.dp, bottom = 0.dp, start = 8.dp, end = 8.dp)) {
                        Text(
                            text,
                            modifier = LayoutGravity.Center,
                            style = MaterialTheme.typography().button,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                        if (graphics != null) {
                            Spacer(LayoutWidth(8.dp))
                            Box(LayoutGravity.Center + LayoutSize(24.dp) + ImagePainter(graphics).toModifier())
                        }
                    }
                }
            }
        }
    }
}

/*
@Composable
fun OutlineButton(
    text: String,
    modifier: Modifier,
    onClick: () -> Unit = {},
    borderWidth: Dp,
    borderColor: Color
) {

}*/
