package com.gitlab.andrewkuryan.lab1.activity

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.ProgressBar
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.gitlab.andrewkuryan.lab1.R

const val TRAILER_URI_EXTRA = "trailerUri"

class TrailerActivity : AppCompatActivity() {

    private lateinit var trailerView: VideoView
    private lateinit var trailerProgress: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trailer)

        val trailerUri = intent.extras?.getString(TRAILER_URI_EXTRA)

        trailerView = findViewById(R.id.trailerView)
        trailerProgress = findViewById(R.id.trailerProgress)

        trailerView.setOnPreparedListener {
            trailerProgress.visibility = View.INVISIBLE
        }

        if (trailerUri != null) {
            trailerProgress.visibility = View.VISIBLE
            val mediaController = MediaController(this)
            trailerView.setMediaController(mediaController)
            trailerView.setVideoURI(Uri.parse(trailerUri))
            trailerView.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        trailerView.stopPlayback()
    }
}