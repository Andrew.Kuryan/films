package com.gitlab.andrewkuryan.lab1.view

import android.content.res.Configuration
import androidx.compose.Composable
import androidx.compose.Providers
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.graphics.Color
import androidx.ui.layout.LayoutGravity
import androidx.ui.layout.LayoutPadding
import androidx.ui.layout.Stack
import androidx.ui.material.*
import androidx.ui.res.colorResource
import androidx.ui.res.stringResource
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontFamily
import androidx.ui.text.font.FontWeight
import androidx.ui.text.style.TextDecoration
import androidx.ui.text.style.TextOverflow
import androidx.ui.unit.TextUnit.Companion.Sp
import androidx.ui.unit.dp
import com.gitlab.andrewkuryan.lab1.R
import com.gitlab.andrewkuryan.lab1.activity.Connect
import com.gitlab.andrewkuryan.lab1.application.actions.*
import com.gitlab.andrewkuryan.lab1.application.reducers.*
import com.gitlab.andrewkuryan.lab1.entity.Film
import java.util.*

@Composable
fun MainView(
    Connect: Connect<AppState>,
    loadImageAction: (Film) -> Unit,
    loadFilmsAction: () -> Unit,
    selectFilmAction: (Film) -> Unit,
    resetFilmsErrorAction: () -> Unit,
    navigateToAction: (Route) -> Unit,
    navigateBackAction: () -> Unit,
    setLanguageAction: (Language) -> Unit,
    setFontSizeAction: (FontSize) -> Unit,
    setFontStyleAction: (FontStyle) -> Unit,
    setThemeAction: (Theme) -> Unit,
    setMinYearAction: (Int) -> Unit,
    setMaxYearAction: (Int) -> Unit,
    setMinRatingAction: (Float) -> Unit,
    setMaxRatingAction: (Float) -> Unit,
    setUserCountriesAction: (List<String>) -> Unit,
    setUserGenresAction: (List<String>) -> Unit,
    setUserProductionAction: (List<String>) -> Unit,
    startTrailerActivityAction: (String) -> Unit
) {
    val scrollerPosition = ScrollerPosition()

    val primaryColor = colorResource(R.color.colorPrimary)
    val primaryDarkColor = colorResource(R.color.colorPrimaryDark)
    val primaryBackLightColor = colorResource(R.color.colorPrimaryBackLight)
    val primaryBackDarkColor = colorResource(R.color.colorPrimaryBackDark)
    val darkGrayColor = colorResource(R.color.colorDarkGray)
    val lightGrayColor = colorResource(R.color.colorLightGray)
    val accentColor = colorResource(R.color.colorAccent)

    Connect({ newState, prevState ->
        newState.settingsState != prevState.settingsState
    }) { configuredState ->
        MaterialTheme(
            colors = when (configuredState.settingsState.theme.name) {
                "dark" -> darkColorPalette(
                    primary = primaryDarkColor,
                    primaryVariant = primaryColor,
                    background = primaryBackDarkColor,
                    secondary = lightGrayColor,
                    surface = Color.Black
                )
                else -> lightColorPalette(
                    primary = primaryColor,
                    primaryVariant = primaryDarkColor,
                    background = primaryBackLightColor,
                    secondary = darkGrayColor,
                    surface = Color.White
                )
            },
            typography = with(configuredState.settingsState) {
                Typography(
                    h1 = textStyle(22, Color.White.toColorPair()),
                    h2 = textStyle(20),
                    h3 = textStyle(18),
                    h4 = textStyle(16),
                    body1 = textStyle(16, darkGrayColor toDark lightGrayColor),
                    body2 = textStyle(18, accentColor.toColorPair()),
                    subtitle1 = textStyle(16).copy(
                        fontStyle = androidx.ui.text.font.FontStyle.Italic,
                        textDecoration = TextDecoration.Underline
                    ),
                    subtitle2 = textStyle(18, darkGrayColor toDark lightGrayColor),
                    caption = textStyle(20).copy(fontWeight = FontWeight.W500),
                    button = textStyle(16, Color.White.toColorPair())
                )
            }
        ) {
            val context = ContextAmbient.current
            val conf = Configuration(context.resources.configuration)
            conf.setLocale(Locale(configuredState.settingsState.language.code))

            Providers(ContextAmbient provides context.createConfigurationContext(conf)) {
                Connect(
                    { newState, prevState ->
                        newState.navigationState.currentRoute != prevState.navigationState.currentRoute
                    }) {
                    Stack {
                        Router(
                            FilmListViewWrapper = @Composable {
                                FilmListView(
                                    loadFilmsAction = loadFilmsAction,
                                    loadImageAction = loadImageAction,
                                    navigateToAction = navigateToAction,
                                    selectFilmAction = selectFilmAction,
                                    Connect = Connect,
                                    scrollerPosition = scrollerPosition
                                )
                            },
                            FilmDetailsViewWrapper = @Composable {
                                Connect({ newState, prevState ->
                                    newState.filmListState.selectedFilm != prevState.filmListState.selectedFilm
                                }) { st ->
                                    if (st.filmListState.selectedFilm != null) {
                                        FilmDetailsView(
                                            navigateBackAction = navigateBackAction,
                                            loadImageAction = loadImageAction,
                                            selectedFilm = st.filmListState.selectedFilm,
                                            startTrailerActivityAction = startTrailerActivityAction
                                        )
                                    }
                                }
                            },
                            FilterViewWrapper = @Composable {
                                FilterView(
                                    Connect = Connect,
                                    setMinYearAction = setMinYearAction,
                                    setMaxYearAction = setMaxYearAction,
                                    navigateBackAction = navigateBackAction,
                                    setMinRatingAction = setMinRatingAction,
                                    setMaxRatingAction = setMaxRatingAction,
                                    setUserCountriesAction = setUserCountriesAction,
                                    setUserGenresAction = setUserGenresAction,
                                    setUserProductionAction = setUserProductionAction
                                )
                            },
                            SettingsViewWrapper = @Composable {
                                SettingsView(
                                    navigateBackAction = navigateBackAction,
                                    settingsState = configuredState.settingsState,
                                    setLanguageAction = setLanguageAction,
                                    setFontSizeAction = setFontSizeAction,
                                    setFontStyleAction = setFontStyleAction,
                                    setThemeAction = setThemeAction
                                )
                            },
                            navigationState = it.navigationState
                        )
                        Connect(
                            { newState, prevState ->
                                newState.filmListState.error != prevState.filmListState.error
                            }
                        ) { stateWithError ->
                            if (stateWithError.filmListState.error != null) {
                                Snackbar(
                                    modifier = LayoutPadding(10.dp) + LayoutGravity.BottomCenter,
                                    text = {
                                        Text(
                                            stateWithError.filmListState.error.localizedMessage
                                                ?: stringResource(R.string.defaultErrorMessage),
                                            style = MaterialTheme.typography().h2,
                                            maxLines = 1,
                                            overflow = TextOverflow.Ellipsis
                                        )
                                    },
                                    action = {
                                        TextButton(onClick = { resetFilmsErrorAction() }) {
                                            Text(stringResource(R.string.errorResetButtonText))
                                        }
                                    })
                            }
                        }
                    }
                }
            }
        }
    }
}

data class ColorPair(val light: Color, val dark: Color) {

    constructor(light: Color) : this(light, light)

    operator fun get(theme: Theme) = when (theme.name) {
        "dark" -> dark
        else -> light
    }
}

infix fun Color.toDark(other: Color) = ColorPair(this, other)

fun Color.toColorPair() = ColorPair(this)

private fun SettingsState.textStyle(fontSize: Int, colors: ColorPair? = null) =
    TextStyle(
        fontSize = Sp(this.fontSize.percent * fontSize),
        fontFamily = when (this.fontStyle.name) {
            "sans_serif" -> FontFamily.SansSerif
            "serif" -> FontFamily.Serif
            "monospace" -> FontFamily.Monospace
            "cursive" -> FontFamily.Cursive
            else -> FontFamily.Default
        }
    ).let { if (colors != null) it.copy(color = colors[theme]) else it }

@Composable
fun Router(
    FilmListViewWrapper: @Composable() () -> Unit,
    FilmDetailsViewWrapper: @Composable() () -> Unit,
    FilterViewWrapper: @Composable() () -> Unit,
    SettingsViewWrapper: @Composable() () -> Unit,
    navigationState: NavigationState
) {
    when (navigationState.currentRoute) {
        FILM_LIST_VIEW_ROUTE -> FilmListViewWrapper()
        FILM_DETAILS_VIEW_ROUTE -> FilmDetailsViewWrapper()
        FILTER_VIEW_ROUTE -> FilterViewWrapper()
        SETTINGS_VIEW_ROUTE -> SettingsViewWrapper()
    }
}