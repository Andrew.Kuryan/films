package com.gitlab.andrewkuryan.lab1.application.actions

import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetFontSize
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetFontStyle
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetLanguage
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SetTheme
import com.gitlab.andrewkuryan.lab1.application.reducers.*
import org.reduxkotlin.Store

fun setLanguage(store: Store<AppState>): (Language) -> Unit {
    return { language ->
        store.dispatch(SetLanguage(language))
    }
}

fun setFontSize(store: Store<AppState>): (FontSize) -> Unit {
    return { fontSize ->
        store.dispatch(SetFontSize(fontSize))
    }
}

fun setFontStyle(store: Store<AppState>): (FontStyle) -> Unit {
    return { fontStyle ->
        store.dispatch(SetFontStyle(fontStyle))
    }
}

fun setTheme(store: Store<AppState>): (Theme) -> Unit {
    return { theme ->
        store.dispatch(SetTheme(theme))
    }
}