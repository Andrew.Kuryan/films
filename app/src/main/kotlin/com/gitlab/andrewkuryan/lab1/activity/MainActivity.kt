package com.gitlab.andrewkuryan.lab1.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.onPreCommit
import androidx.compose.state
import androidx.ui.core.setContent
import com.appspot.magtech.httplogger.HttpLogger
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.get
import com.github.salomonbrys.kotson.jsonObject
import com.github.salomonbrys.kotson.registerTypeAdapter
import com.gitlab.andrewkuryan.lab1.application.actions.*
import com.gitlab.andrewkuryan.lab1.application.middleware.loggerMiddleware
import com.gitlab.andrewkuryan.lab1.application.reducers.*
import com.gitlab.andrewkuryan.lab1.entity.Film
import com.gitlab.andrewkuryan.lab1.view.MainView
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.GsonBuilder
import org.reduxkotlin.Store
import org.reduxkotlin.applyMiddleware
import org.reduxkotlin.createStore

typealias Connect<T> = @Composable() (shouldUpdate: (nextState: T, prevState: T) -> Boolean, child: @Composable() (T) -> Unit) -> Unit

class MainActivity : AppCompatActivity() {

    private val logger = HttpLogger("http://192.168.100.5")
    private val database = FirebaseDatabase.getInstance()

    private val gson = GsonBuilder()
        .registerTypeAdapter<IntRange> {
            serialize { jsonObject("from" to it.src.first, "to" to it.src.last) }
            deserialize { it.json["from"].asInt..it.json["to"].asInt }
        }
        .registerTypeAdapter<ClosedFloatingPointRange<Float>> {
            serialize { jsonObject("from" to it.src.start, "to" to it.src.endInclusive) }
            deserialize { it.json["from"].asFloat..it.json["to"].asFloat }
        }
        .serializeNulls()
        .create()

    private lateinit var store: Store<AppState>

    // dispatch state to props
    private lateinit var initFilmListAction: (FilmListState) -> Unit
    private lateinit var loadFilmsAction: () -> Unit
    private lateinit var loadFilmsErrorAction: (Exception) -> Unit
    private lateinit var loadImageAction: (Film) -> Unit
    private lateinit var selectFilmAction: (Film) -> Unit
    private lateinit var resetFilmsErrorAction: () -> Unit

    private lateinit var navigateToAction: (Route) -> Unit
    private lateinit var navigateWithReplaceAction: (Route) -> Unit
    private lateinit var navigateBackAction: () -> Unit

    private lateinit var setLanguageAction: (Language) -> Unit
    private lateinit var setFontSizeAction: (FontSize) -> Unit
    private lateinit var setFontStyleAction: (FontStyle) -> Unit
    private lateinit var setThemeAction: (Theme) -> Unit

    private lateinit var setMinYearAction: (Int) -> Unit
    private lateinit var setMaxYearAction: (Int) -> Unit
    private lateinit var setMinRatingAction: (Float) -> Unit
    private lateinit var setMaxRatingAction: (Float) -> Unit
    private lateinit var setUserCountriesAction: (List<String>) -> Unit
    private lateinit var setUserGenresAction: (List<String>) -> Unit
    private lateinit var setUserProductionAction: (List<String>) -> Unit

    private lateinit var connect: Connect<AppState>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val initState = if (savedInstanceState?.getString("state") != null) {
            gson.fromJson(savedInstanceState.getString("state")!!)
        } else AppState()

        val isFetching = initState.filmListState.films.isEmpty()

        store = createStore(
            rootReducer,
            initState.copy(filmListState = initState.filmListState.copy(isFetching = isFetching)),
            applyMiddleware(loggerMiddleware(logger))
        )

        loadImageAction = loadImage(store)
        loadFilmsErrorAction = loadFilmsError(store)
        loadFilmsAction = loadFilms(store, database, loadFilmsErrorAction)
        selectFilmAction = selectFilm(store)
        resetFilmsErrorAction = resetFilmsError(store)
        initFilmListAction = initFilmList(loadFilmsAction)

        navigateToAction = navigateTo(store)
        navigateWithReplaceAction = navigateWithReplace(store)
        navigateBackAction = navigateBack(store)

        setLanguageAction = setLanguage(store)
        setFontSizeAction = setFontSize(store)
        setFontStyleAction = setFontStyle(store)
        setThemeAction = setTheme(store)

        setMinYearAction = setMinYear(store)
        setMaxYearAction = setMaxYear(store)
        setMinRatingAction = setMinRating(store)
        setMaxRatingAction = setMaxRating(store)
        setUserCountriesAction = setUserCountries(store)
        setUserGenresAction = setUserGenres(store)
        setUserProductionAction = setUserProduction(store)

        connect =
            (@Composable { shouldUpdate: (nextState: AppState, prevState: AppState) -> Boolean,
                           child: @Composable() (AppState) -> Unit ->
                val (model, setModel) = state { store.state }

                onPreCommit {
                    val unsubscribe = store.subscribe {
                        if (shouldUpdate(store.state, model)) {
                            setModel(store.state)
                        }
                    }
                    this.onDispose(unsubscribe)
                }

                child(model)
            })

        setContent {
            MainView(
                Connect = connect,
                loadImageAction = loadImageAction,
                loadFilmsAction = loadFilmsAction,
                selectFilmAction = selectFilmAction,
                resetFilmsErrorAction = resetFilmsErrorAction,
                navigateToAction = navigateToAction,
                navigateBackAction = navigateBackAction,
                setLanguageAction = setLanguageAction,
                setFontSizeAction = setFontSizeAction,
                setFontStyleAction = setFontStyleAction,
                setThemeAction = setThemeAction,
                setMinYearAction = setMinYearAction,
                setMaxYearAction = setMaxYearAction,
                setMinRatingAction = setMinRatingAction,
                setMaxRatingAction = setMaxRatingAction,
                setUserCountriesAction = setUserCountriesAction,
                setUserGenresAction = setUserGenresAction,
                setUserProductionAction = setUserProductionAction,
                startTrailerActivityAction = { uri ->
                    val intent = Intent(this, TrailerActivity::class.java)
                    intent.putExtra(TRAILER_URI_EXTRA, uri)
                    startActivity(intent)
                }
            )
        }

        initFilmListAction(initState.filmListState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("state", gson.toJson(store.state))
    }

    override fun onBackPressed() {
        navigateBackAction()
    }
}