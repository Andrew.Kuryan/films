package com.gitlab.andrewkuryan.lab1.view.extensions

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.core.toModifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Clickable
import androidx.ui.graphics.painter.ImagePainter
import androidx.ui.layout.LayoutPadding
import androidx.ui.layout.LayoutSize
import androidx.ui.material.ripple.Ripple
import androidx.ui.res.imageResource
import androidx.ui.unit.dp

@Composable
fun ImageButton(
    id: Int,
    onClick: () -> Unit = {},
    modifier: Modifier = LayoutSize(36.dp) + LayoutPadding(6.dp)
) {
    val image = imageResource(id)
    Ripple(bounded = true) {
        Clickable(onClick = onClick) {
            Box(modifier + ImagePainter(image).toModifier())
        }
    }
}