package com.gitlab.andrewkuryan.lab1.application.actiontypes

import com.gitlab.andrewkuryan.lab1.entity.Film

sealed class FilterAction : AppAction()

data class InitFilter(val filmList: List<Film>) : FilterAction()

data class SetMinYear(val year: Int) : FilterAction()

data class SetMaxYear(val year: Int) : FilterAction()

data class SetMinRating(val rating: Float) : FilterAction()

data class SetMaxRating(val rating: Float) : FilterAction()

data class SetUserCountries(val countries: List<String>) : FilterAction()

data class SetUserGenres(val genres: List<String>) : FilterAction()

data class SetUserProduction(val production: List<String>) : FilterAction()