package com.gitlab.andrewkuryan.lab1.application.reducers

import com.gitlab.andrewkuryan.lab1.application.actiontypes.FilmListAction
import com.gitlab.andrewkuryan.lab1.application.actiontypes.FilterAction
import com.gitlab.andrewkuryan.lab1.application.actiontypes.NavigationAction
import com.gitlab.andrewkuryan.lab1.application.actiontypes.SettingsAction
import org.reduxkotlin.Reducer

data class AppState(
    val filmListState: FilmListState = FilmListState(),
    val navigationState: NavigationState = NavigationState(),
    val settingsState: SettingsState = SettingsState(),
    val filterState: FilterState = FilterState()
)

val rootReducer: Reducer<AppState> = { state, action ->
    when (action) {
        is FilmListAction -> state.copy(
            filmListState = filmListReducer(
                state.filmListState,
                action
            )
        )
        is NavigationAction -> state.copy(
            navigationState = navigationReducer(
                state.navigationState,
                action
            )
        )
        is SettingsAction -> state.copy(
            settingsState = settingsReducer(
                state.settingsState,
                action
            )
        )
        is FilterAction -> state.copy(
            filterState = filterReducer(
                state.filterState,
                action
            )
        )
        else -> state
    }
}