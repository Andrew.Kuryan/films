package com.gitlab.andrewkuryan.lab1.application.actions

import com.gitlab.andrewkuryan.lab1.application.actiontypes.*
import com.gitlab.andrewkuryan.lab1.application.reducers.AppState
import org.reduxkotlin.Store

fun setMinYear(store: Store<AppState>): (Int) -> Unit {
    return { year ->
        store.dispatch(SetMinYear(year))
    }
}

fun setMaxYear(store: Store<AppState>): (Int) -> Unit {
    return { year ->
        store.dispatch(SetMaxYear(year))
    }
}

fun setMinRating(store: Store<AppState>): (Float) -> Unit {
    return { rating ->
        store.dispatch(SetMinRating(rating))
    }
}

fun setMaxRating(store: Store<AppState>): (Float) -> Unit {
    return { rating ->
        store.dispatch(SetMaxRating(rating))
    }
}

fun setUserCountries(store: Store<AppState>): (List<String>) -> Unit {
    return { countries ->
        store.dispatch(SetUserCountries(countries))
    }
}

fun setUserGenres(store: Store<AppState>): (List<String>) -> Unit {
    return { genres ->
        store.dispatch(SetUserGenres(genres))
    }
}

fun setUserProduction(store: Store<AppState>): (List<String>) -> Unit {
    return { production ->
        store.dispatch(SetUserProduction(production))
    }
}