plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    kotlin("android")
    kotlin("android.extensions")
    id("kotlin-android")
    id("kotlin-android-extensions")
    kotlin("plugin.serialization") version "1.3.61"
}

android {
    compileSdkVersion(29)
    buildToolsVersion("29.0.2")
    defaultConfig {
        applicationId = "com.gitlab.andrewkuryan.lab1"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "0.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerVersion = "1.3.61-dev-withExperimentalGoogleExtensions-20200129"
        kotlinCompilerExtensionVersion = "0.1.0-dev06"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    sourceSets {
        val kotlinAddintionalSourceSets = project.file("src/main/kotlin")
        findByName("main")?.java?.srcDirs(kotlinAddintionalSourceSets)

        val kotlinTestSourceSets = project.file("src/test/kotlin")
        findByName("test")?.java?.srcDirs(kotlinTestSourceSets)
    }
}

dependencies {
    implementation(kotlin("stdlib"))

    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.core:core-ktx:1.2.0")

    implementation("org.reduxkotlin:redux-kotlin:0.3.2")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.3")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.3")

    implementation("com.google.firebase:firebase-database-ktx:19.2.1")
    implementation("com.appspot.magtech:http-logger:0.1.0")
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")

    implementation("androidx.constraintlayout:constraintlayout:2.0.0-beta4")

    implementation("androidx.ui:ui-tooling:0.1.0-dev06")
    implementation("androidx.ui:ui-layout:0.1.0-dev06")
    implementation("androidx.ui:ui-material:0.1.0-dev06")
    implementation("androidx.ui:ui-text:0.1.0-dev06")

    testImplementation("junit:junit:4.12")
    testImplementation("androidx.test:core:1.2.0")
}