module.exports = [
    {
        title: "Star Wars: Episode IX - The Rise of Skywalker",
        trailer: "https://firebasestorage.googleapis.com/v0/b/lab1-e7222.appspot.com/o/Star%20Wars%3A%20Episode%20IX%20-%20The%20Rise%20of%20Skywalker.mp4?alt=media&token=7d3fbfdb-aa8a-4c65-ab1c-031c6ed482d4"
    },
    {
        title: "The Hobbit: An Unexpected Journey"
    },
    {
        title: "Mission: Impossible - Fallout",
        trailer: "https://firebasestorage.googleapis.com/v0/b/lab1-e7222.appspot.com/o/Mission%3A%20Impossible%20-%20Fallout.mp4?alt=media&token=9ecc13cc-f110-4b16-8db6-da8e484a1015"
    },
    {
        title: "Inception"
    },
    {
        title: "Skyfall"
    },
    {
        title: "Avatar"
    },
    {
        title: "The Lord of the Rings: The Fellowship of the Ring"
    },
    {
        title: "Interstellar"
    },
    {
        title: "Pirates of the Caribbean: At World's End",
        trailer: "https://firebasestorage.googleapis.com/v0/b/lab1-e7222.appspot.com/o/Pirates%20of%20the%20Caribbean%3A%20At%20World's%20End.mp4?alt=media&token=c5c5fa49-2354-413c-9519-38190731f1f7"
    },
    {
        title: "Transformers: Age of Extinction"
    }
];
