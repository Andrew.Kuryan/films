const path = require('path');
const admin = require("firebase-admin");
const fetch = require("node-fetch");
const films = require("./data");

const serviceAccount = require(path.resolve(__dirname, "lab1-e7222-firebase-adminsdk-whd6b-85503d5194.json"));

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://lab1-e7222.firebaseio.com/"
});

const db = admin.database();
const ref = db.ref("/");

const apiUrl = (title) => `http://www.omdbapi.com/?apikey=d2b995af&t=${title}&plot=full`;

Promise.all(films
    .map(film => fetch(apiUrl(film.title))
        .then(res => res.json())
        .then(data => ({
            ...data,
            Trailer: film.trailer || ""
        })))
)
    .then(filmData => ref.set(filmData))
    .then(res => {
        console.log(res);
    });
